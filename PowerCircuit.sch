EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:LM7805_TO220 U3
U 1 1 60BA3419
P 5450 2750
F 0 "U3" H 5450 2992 50  0000 C CNN
F 1 "LM7805_TO220" H 5450 2901 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-H5_Horizontal" H 5450 2975 50  0001 C CIN
F 3 "https://www.onsemi.cn/PowerSolutions/document/MC7800-D.PDF" H 5450 2700 50  0001 C CNN
	1    5450 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60BA4096
P 4600 2900
F 0 "C2" H 4715 2946 50  0000 L CNN
F 1 "100nF" H 4715 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 4638 2750 50  0001 C CNN
F 3 "~" H 4600 2900 50  0001 C CNN
	1    4600 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 60BA461F
P 6250 2900
F 0 "C3" H 6368 2946 50  0000 L CNN
F 1 "22uF" H 6368 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 6288 2750 50  0001 C CNN
F 3 "~" H 6250 2900 50  0001 C CNN
	1    6250 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 60BA4F4B
P 4100 2900
F 0 "C1" H 4218 2946 50  0000 L CNN
F 1 "220uf" H 4218 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 4138 2750 50  0001 C CNN
F 3 "~" H 4100 2900 50  0001 C CNN
	1    4100 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 60BA5426
P 6950 2900
F 0 "C4" H 7065 2946 50  0000 L CNN
F 1 "100nF" H 7065 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 6988 2750 50  0001 C CNN
F 3 "~" H 6950 2900 50  0001 C CNN
	1    6950 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2750 4600 2750
Connection ~ 4600 2750
Wire Wire Line
	4600 2750 4100 2750
Wire Wire Line
	5750 2750 6250 2750
Connection ~ 6250 2750
Wire Wire Line
	6250 2750 6950 2750
Wire Wire Line
	5450 3050 5450 3250
Connection ~ 5450 3250
Wire Wire Line
	5450 3250 6250 3250
Wire Wire Line
	6250 3050 6250 3250
Connection ~ 6250 3250
Wire Wire Line
	6250 3250 6950 3250
Wire Wire Line
	6950 3050 6950 3250
Wire Wire Line
	6950 2750 7200 2750
Wire Wire Line
	4600 3050 4600 3250
Connection ~ 4600 3250
Wire Wire Line
	4600 3250 5450 3250
Wire Wire Line
	4100 3050 4100 3250
Wire Wire Line
	4100 3250 4600 3250
Wire Wire Line
	4100 2750 3850 2750
Connection ~ 4100 2750
Text GLabel 3850 2750 0    50   Input ~ 0
Vin
Connection ~ 6950 2750
Text Notes 7500 7500 0    50   ~ 0
Power Circuit Schematic
Text Notes 8200 7650 0    50   ~ 0
04/06/21
Text HLabel 7200 2750 2    50   Input ~ 0
GPIO6
$Comp
L power:GND #PWR?
U 1 1 60BCAFB2
P 5450 3600
F 0 "#PWR?" H 5450 3350 50  0001 C CNN
F 1 "GND" H 5455 3427 50  0000 C CNN
F 2 "" H 5450 3600 50  0001 C CNN
F 3 "" H 5450 3600 50  0001 C CNN
	1    5450 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3250 5450 3600
$EndSCHEMATC
