EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM741 U1
U 1 1 60BA22E5
P 3800 3600
F 0 "U1" H 4144 3554 50  0000 L CNN
F 1 "LM741" H 4144 3645 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:Infineon_PG-HSOF-8-1" H 3850 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 3950 3750 50  0001 C CNN
	1    3800 3600
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM741 U2
U 1 1 60BA3FE9
P 5850 3700
F 0 "U2" H 6194 3654 50  0000 L CNN
F 1 "LM741" H 6194 3745 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:Infineon_PG-HSOF-8-1" H 5900 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 6000 3850 50  0001 C CNN
	1    5850 3700
	1    0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 60BA680F
P 4750 3600
F 0 "R3" V 4543 3600 50  0000 C CNN
F 1 "100K" V 4634 3600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4680 3600 50  0001 C CNN
F 3 "~" H 4750 3600 50  0001 C CNN
	1    4750 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60BA83B0
P 5750 2950
F 0 "R6" V 5543 2950 50  0000 C CNN
F 1 "100K" V 5634 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5680 2950 50  0001 C CNN
F 3 "~" H 5750 2950 50  0001 C CNN
	1    5750 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 60BA92B9
P 5550 4450
F 0 "R5" H 5620 4496 50  0000 L CNN
F 1 "100K" H 5620 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5480 4450 50  0001 C CNN
F 3 "~" H 5550 4450 50  0001 C CNN
	1    5550 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60BAA3C0
P 5050 4450
F 0 "R4" H 5120 4496 50  0000 L CNN
F 1 "300K" H 5120 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4980 4450 50  0001 C CNN
F 3 "~" H 5050 4450 50  0001 C CNN
	1    5050 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60BAACF6
P 3050 3500
F 0 "R1" V 2843 3500 50  0000 C CNN
F 1 "22K" V 2934 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2980 3500 50  0001 C CNN
F 3 "~" H 3050 3500 50  0001 C CNN
	1    3050 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60BAB83C
P 3700 2950
F 0 "R2" V 3493 2950 50  0000 C CNN
F 1 "3K" V 3584 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3630 2950 50  0001 C CNN
F 3 "~" H 3700 2950 50  0001 C CNN
	1    3700 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 3600 4450 3600
Wire Wire Line
	4900 3600 5300 3600
Wire Wire Line
	5550 3800 5550 4300
Wire Wire Line
	5550 3800 5050 3800
Wire Wire Line
	5050 3800 5050 4300
Connection ~ 5550 3800
Wire Wire Line
	5300 3600 5300 2950
Wire Wire Line
	5300 2950 5600 2950
Connection ~ 5300 3600
Wire Wire Line
	5300 3600 5550 3600
Wire Wire Line
	5900 2950 6500 2950
Wire Wire Line
	6500 2950 6500 3700
Wire Wire Line
	6500 3700 6150 3700
Wire Wire Line
	4450 3600 4450 2950
Wire Wire Line
	4450 2950 3850 2950
Connection ~ 4450 3600
Wire Wire Line
	4450 3600 4600 3600
Wire Wire Line
	3350 3500 3350 2950
Wire Wire Line
	3350 2950 3550 2950
Wire Wire Line
	3500 3500 3350 3500
Connection ~ 3350 3500
Wire Wire Line
	3350 3500 3200 3500
Text GLabel 2500 3500 0    50   Input ~ 0
Vin
Text GLabel 2500 4000 0    50   Input ~ 0
+5V
Wire Wire Line
	2500 3500 2900 3500
Wire Wire Line
	3700 3300 3700 3200
Wire Wire Line
	3700 3200 2500 3200
Wire Wire Line
	3700 3200 5750 3200
Wire Wire Line
	5750 3200 5750 3400
Connection ~ 3700 3200
Wire Wire Line
	2500 4000 3700 4000
Wire Wire Line
	3700 4000 3700 3900
Wire Wire Line
	3700 4000 5750 4000
Connection ~ 3700 4000
Text GLabel 2500 4750 0    50   Input ~ 0
3,3V
Wire Wire Line
	2500 4750 5050 4750
Wire Wire Line
	5050 4750 5050 4600
Wire Wire Line
	5550 5000 5550 4600
Wire Wire Line
	3500 3700 3350 3700
Wire Wire Line
	3350 3700 3350 5000
Wire Wire Line
	3350 5000 4250 5000
NoConn ~ 5850 3400
NoConn ~ 5950 3400
NoConn ~ 3900 3300
NoConn ~ 3800 3300
Text Notes 7400 7500 0    50   ~ 0
Nathan Mboko
Text Notes 8450 7650 0    50   ~ 0
2021/06/04
Text HLabel 7100 3700 2    50   Input ~ 0
GPIO5
Wire Wire Line
	6500 3700 7100 3700
Connection ~ 6500 3700
$Comp
L power:GND #PWR?
U 1 1 60BC9C75
P 4250 5250
F 0 "#PWR?" H 4250 5000 50  0001 C CNN
F 1 "GND" H 4255 5077 50  0000 C CNN
F 2 "" H 4250 5250 50  0001 C CNN
F 3 "" H 4250 5250 50  0001 C CNN
	1    4250 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5250 4250 5000
Connection ~ 4250 5000
Wire Wire Line
	4250 5000 5550 5000
Text GLabel 2500 3200 0    50   Input ~ 0
-5V
$EndSCHEMATC
